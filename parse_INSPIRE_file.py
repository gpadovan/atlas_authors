# parse_INSPIRE_file.py
# converts the inspire file to a latex
#
# author: G. Padovano - CERN
# mailto: giovanni.padovano@cern.ch


import sys
from argparse import ArgumentParser

import re

def parse_INSPIRE_file(
        inputfile,
        outputfile,
        npapers
):
    
    # read input file and build dictionary with papers information
    
    fin = open(inputfile, "r")
    lines = [l.strip() for l in fin.readlines()]
    
    
    blocks = []
    block = ''
    for l in lines:
        if not l=='':
            block = block + l
        else:
            blocks.append(block)
            block = ''
    
    
    papers = []
    for block in blocks:
        
        title, review, arXiv = re.match('.*cite{.*%``(.*),\'\'(.*)\[(arXiv.*\])\]',block).groups()
        
        if not review == '':
            remove, add = re.match('.*(textbf{(.*)})',review).groups()
            review = review.replace('\\'+str(remove),str(add))
        
            
        
        paper = {'title'  : title,
                 'review' : review,
                 'arXiv'  : arXiv }
        
        
        papers.append(paper)
    
    fin.close()
    
    # write output .tex file
    fout = open(outputfile, "w")

    if not npapers==9999:
        papers = papers[:npapers]
    
    for i,paper in enumerate(papers):
        fout.write("\ecvitem{["+str(i+1)+"]}")
        fout.write("{")
        fout.write(paper['title']+", ")
        if not paper['review']=='':
            fout.write(paper['review']+", ")
        fout.write("\\texttt{"+paper['arXiv']+"}")
        fout.write("}")
        fout.write("\n")
    
    fout.close()
    
    
    return



def parse_args(argv):
    parser = ArgumentParser(description='Generates latex from INSPIRE')
    parser.add_argument('inputfile', type=str, help='input files with papers list as downloaded from INSPIRE')
    parser.add_argument('-o', '--outputfile', type=str, default='./output.txt', help='output file with tex code')
    parser.add_argument('-n', '--npapers', type=int, default=9999, help='take the firt n papers only')
    args = parser.parse_args(argv)
    return vars(args)


if __name__ == '__main__':
    sys.exit(parse_INSPIRE_file(**parse_args(sys.argv[1:])))
