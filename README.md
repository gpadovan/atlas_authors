# ATLAS authors and papers

## ATLAS authorship for IRIS

This code converts ATLAS authorlists in .tex in a string compatible with IRIS database syntax.

Usage:
- go to ATLAS public results [here](https://twiki.cern.ch/twiki/bin/view/AtlasPublic), look for the paper, download the authorlist file in .tex from internal GitLab reopository (accessible via Glance)

- run the script to generate the IRIS string

Example run:
```
python3 get_authors_for_IRIS.py atlas_authlist.tex -o output.txt --rome-only
```
## Papers list from INSPIRE to EuropassCV

This code converts the list of papers as downloaded from IRIS into a .tex file compatible with LaTeX EuropassCV
Usage:
- got ot IRIS and download the list of papaers from the author's page

- run the script to generate the .tex file

Example run:
```
python3 parse_INSPIRE_file.py ./INSPIRE-CiteAll.tex -o paper_list.tex
```
Use option ```-n 10``` to convert only the first 10 papers