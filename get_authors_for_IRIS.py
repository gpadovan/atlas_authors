# get_authors_for_IRIS.py
# converts ATLAS authorlist in .tex (available on GitLab from GLANCE db) into authors string to be inserted in IRIS database


import sys
from argparse import ArgumentParser

import re

def get_authors_for_IRIS(
    inputfile="",
    outputfile="",
    rome_only=False
):

    print("reading authors from: {0}".format(inputfile))
    
    f = open(inputfile, 'r')
    
    lines = f.readlines()
    
    IRIS_string = ""
    authors = []
    for line in lines:
        
        if not "AtlasOrcid" in line: continue
        if rome_only:
            if not "scriptsize 74" in line: continue #74 --> INFN Roma (inclusive as Sapienza authors are also INFN)
        
        
        author = re.match('.*AtlasOrcid(.*)textrm.*$',line).groups()[0]
        author = author.split("]")[-1].split("$")[0][1:-1].replace("~", " ")
        
        authors.append(author)        
        IRIS_string = IRIS_string + author + ", "
        
    # remove spurius characters from IRIS string
    IRIS_string = IRIS_string[0:-2]
    
    IRIS_string = IRIS_string.replace("{","")
    IRIS_string = IRIS_string.replace("}","")
    IRIS_string = IRIS_string.replace("\\","")
    IRIS_string = IRIS_string.replace("^","")
    IRIS_string = IRIS_string.replace("\'","")
    IRIS_string = IRIS_string.replace("\"","")
    
    
    
    # print and save on file
    print("List of the authors for this ATLAS paper:")
    for author in authors:
        print("author: {0}".format(author))
    print("Total number of authors: {0}".format(len(authors)))
    
        
    print("---------------- IRIS string -----------------")
    print(IRIS_string)
    
    of = open(outputfile, "w")
    of.write(IRIS_string)
    of.close()
    
    f.close()
    
    
    return


def parse_args(argv):
    parser = ArgumentParser(description='Generates IRIS authors string')
    parser.add_argument('inputfile', type=str, help='input file with authorlist in .tex from GitLab')
    parser.add_argument('-o', '--outputfile', type=str, default='./output.txt', help='output file for IRIS string')
    parser.add_argument('-r', '--rome-only', default=False, action='store_true', help='look for authors of INFN Roma and Sapienza university only')    

    args = parser.parse_args(argv)
    return vars(args)


if __name__ == '__main__':
    sys.exit(get_authors_for_IRIS(**parse_args(sys.argv[1:])))
